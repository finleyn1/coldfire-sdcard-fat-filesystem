#ifndef FCLUSTER_H
#define FCLUSTER_H

#include "fat_type.h"

//functions and macros related to cluster management


#define SECTOR_OFF(x)	(((x)/512)%fat_fsinfo.cluster_size)
#define CLUSTER_OFF(x)	((x)/(fat_fsinfo.cluster_size*512))


int free_cluster(uint32 cluster);
uint32 alloc_cluster();

uint32	add_cluster(uint32 cluster);
int rem_cluster(uint32 start);


uint32	next_cluster(uint32 cluster);

uint32 get_cluster_num(uint32 start, uint32 offset);

int put_ind_sector(uint32 cluster, uint32 sector, void *data);
void *get_ind_sector(uint32 cluster, uint32 sector);


#endif
