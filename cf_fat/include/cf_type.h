#ifndef CF_TYPE_H
#define CF_TYPE_H

typedef unsigned long int	uint32;
typedef unsigned short int	uint16;
typedef unsigned char		uint8;

typedef signed int		int32;
typedef signed short int	int16;
typedef signed char		int8;

typedef volatile uint32		vuint32;
typedef volatile uint16		vuint16;
typedef volatile uint8		vuint8;

#endif
