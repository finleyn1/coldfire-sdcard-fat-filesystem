#ifndef FAT_UTIL_H
#define FAT_UTIL_H


#include "fat_type.h"


extern uint32 bswap32(uint32 a);
extern uint16 bswap16(uint16 a);


int init_fat();
int ncmp(char *a, char *b, int n);




#endif
