#ifndef FBLOCK_H
#define FBLOCK_H

#include "cf_type.h"

//sector/block chache related functions


//number of cache entries
#define N_CACHE 8


//flag definition
#define F_DIRTY 2
#define F_VALID 1
	
//a single cache entry
struct fc_ent
{
	uint32 num; //block/sector in entry
	uint32 flag; 
	struct fc_ent *next;
	uint8 sec[512]; //data for sector
};


	
int init_cache();
int sync_cache();



//cache buffered operations
//use these for most FS operations

void *get_sector(uint32 sector);
int put_sector(uint32 sector, void *data);




//non cached opperations below be careful

int read_sector(uint32 sec, uint8 *data);

int write_sector(uint32 sec, uint8 *data);

//writing can mess up cache coherency
//sync cache first if using outside of fblock.c


#endif
