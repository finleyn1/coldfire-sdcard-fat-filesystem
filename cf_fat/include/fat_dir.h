#ifndef FAT_DIR_H
#define FAT_DIR_H

#include "fat_type.h"

int dir_search(FAT_FILE *par,FAT_FILE *fin, char *name);

int fatmkf(FAT_FILE *par, char *name);
int fatmkd(FAT_FILE *par, char *name);

int init_fat_dir(uint32 alcluster, FAT_FILE *par);

int fatrmd(FAT_FILE *fin);
int fatrmf(FAT_FILE *fin);



#endif
