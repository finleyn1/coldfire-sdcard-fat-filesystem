#ifndef FAT_TYPE_H
#define FAT_TYPE_H

#include "cf_type.h"
#include "fblock.h"
#include "fat_util.h"


//FAT filesystem structure definitions

struct fat_fs_info
{
	uint32 cluster_size;
	uint32 fat_start;
	uint32 data_start;
	uint32 root_cluster;
	uint32 table_size;
};



typedef struct fat_BS
{
	uint8	jump[3];
	uint8	ident[8];
	uint16	bytes_per_sector;
	uint8	sectors_per_cluster;
	uint16	reserved_sectors;
	uint8	table_count;
	uint16	root_entries;
	uint16	volume_sectors;
	uint8	media_desc_type;
	uint16	sectors_per_fat;
	uint16	sectors_per_track;
	uint16	heads;
	uint32	hidden_sectors;
	uint32	large_sector_count;
}__attribute__((packed)) FAT_BOOTSEC;


//struct for fat32 extended boot sector
typedef struct fat32_extBS
{
	uint32	table_size;	
	uint16	flags;
	uint16	version;
	uint32	root_cluster;
	uint16	fat_info;
	uint16	backup_BS;
	uint8	reserved_0[12];
	uint8	drive_number;
	uint8	reserved_1;
	uint8	boot_signature;
	uint32	volume_id;
	uint8	volume_label[11];
	uint8	type_label[8];
}__attribute__((packed)) FAT32_EXTB;


typedef struct fat_file_info
{
	uint32	start; //first cluster of file
	uint8	attr;	//file attributes
	uint32	size;	//byte size of file

	uint32	dirsec; //sector the dirent of file found in
	uint32	diroff; //byte offset in that sector
	uint32	parcluster; //starting cluster of pardir
}FAT_FILE;


//struct for a fat32_directory entry
typedef struct fat32_dirent
{
	uint8	name[11];
	uint8	attr;
	uint8	reserved; 
	uint8	creation_time;
	uint16	t_creat;
	uint16	d_creat;
	uint16	d_acc;
	uint16	cluster_high;
	uint16	mod_time;
	uint16	mod_date;
	uint16	cluster_low;
	uint32	size;
}__attribute__((packed)) FAT_DIRENT;


extern struct fat_fs_info fat_fsinfo;
extern FAT_FILE root;
extern FAT_FILE cwd;


#endif
