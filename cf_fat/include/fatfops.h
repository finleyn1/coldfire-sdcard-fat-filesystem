#ifndef FATFOPS_H
#define FATFOPS_H

#include "fat_type.h"


#define N_OPENE 8

#define OF_RD	2
#define OF_WR	4
#define OF_INV	8

#define FSEEK_SET 1
#define FSEEK_CUR 2
#define FSEEK_END 3


typedef struct of_desc
{
	FAT_FILE fd;
	uint32 offset;
	uint32 mode;
} OF_DESC;


void init_oft();

int fat_openf(FAT_FILE *fd, uint32 mode);
int fat_closef(int fd);

int fat_seek(int fd, uint32 offset, uint8 type);

int fat_read(int fd, void *buf, int n);
int fat_write(int fd, void *buf, int n);





int fat_truncate(FAT_FILE *fi, uint32 nsize);
int fat_mod_size(FAT_FILE *FI, int n);














#endif
