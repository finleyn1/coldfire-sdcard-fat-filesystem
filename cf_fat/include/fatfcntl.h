#ifndef FATFCNTL_H
#define FATFCNTL_H

#include "fat_type.h"





int tokenize_path(char *path);
int fatstconv(char *dst, char* src);
int fat_ls(char *path);
int fat_mkdir(char *path);
int fat_creat(char *path);
int fat_rm(char *path);
int fat_rmdir(char *path);
int fat_trunc(char *path,uint32 size);


int fat_open(char *path,uint32 mode);

#define fat_close(x) fat_closef((x));


#endif
