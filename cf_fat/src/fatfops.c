#include "fatfops.h"
#include "fcluster.h"



OF_DESC oft[N_OPENE];

//sets the size of the given file to the specified size
//only sets size of regular files
int fat_truncate(FAT_FILE *fi, uint32 nsize)
{
	int diff;
	
	if((fi->attr&~0x20))
		return 1;

	//calculate delta
	//maybe this could overflow...
	diff = (nsize)-fi->size;

	fat_mod_size(fi,diff);

}


//modifies the size of the given file by the signed size n
int fat_mod_size(FAT_FILE *fi, int n)
{

	uint32 oldclust;
	uint32 newclust;
	uint32 nsec;
	int    i;
	uint8  *blk;
	FAT_DIRENT *dir;


	//don't need to do anything
	if(!n)
		return 0;


	//calculate how many sectors in the file
	nsec = (fi->size)/512;
	if(fi->size%512)
		nsec++;

	//calcuate how many clusters file has
	oldclust = nsec/fat_fsinfo.cluster_size;
	if(nsec%fat_fsinfo.cluster_size)
		oldclust++;


	//if file size will be neg
	//make zero
	if(n<0 && (-n)>fi->size)
		fi->size=0;
	else
		fi->size+=n;

	
	if(fi->size==0)
		newclust=1;
	else
	{
		nsec = (fi->size)/512;
		if(fi->size%512)
			nsec++;

		//calcluate the new required cluster count
		newclust = nsec/fat_fsinfo.cluster_size;
		if(nsec%fat_fsinfo.cluster_size)
			newclust++;
	}



	
	if(newclust > oldclust) //adding clusters
	{
		for(;oldclust<newclust;oldclust++)
			if(fi->start==0)
				fi->start = alloc_cluster();
			else
				add_cluster(fi->start);
	}
	else if(newclust<oldclust)
	{
		//remove all bust last secotr
		for(;oldclust>newclust;oldclust--)
			rem_cluster(fi->start);
		
		if(fi->size==0)
		{
			free_cluster(fi->start);
			fi->start=0;
		}
	}


	//update parent directory entry

	//get directory entry for file
	blk = get_sector(fi->dirsec);

	dir = (FAT_DIRENT *)(blk+fi->diroff);


	
	//if size is zero,there are no clusters
	if(fi->size==0)
	{
		dir->size=0;
		dir->cluster_high=0;
		dir->cluster_low=0;
	}
	else //write start cluster to parent
	{
		dir->size = bswap32(fi->size);
		dir->cluster_high = bswap16((fi->start>>16)&0xFFFF);	
		dir->cluster_low = bswap16(fi->start&0xFFFF);	
	}
	put_sector(fi->dirsec,blk);

	return 0;
}


void init_oft()
{
	int i=0;
	for(i=0;i<N_OPENE;i++)
		oft[i].mode=OF_INV;
}


int fat_openf(FAT_FILE *fd, uint32 mode)
{
	int i=0;

	for(i=0;i<N_OPENE;i++)
		if(oft[i].mode&OF_INV)
			break;

	if(i==N_OPENE)
		return -1;


	oft[i].mode &= ~OF_INV;
	oft[i].mode |= mode;
	oft[i].offset=0;
	oft[i].fd = *fd;

	return i;
}


int fat_closef(int fd)
{
	if(fd<0)
		return 1;

	oft[fd].mode|=OF_INV;
	return 0;
}


//sets the offset of given file descriptor
int fat_seek(int fd, uint32 offset, uint8 type)
{
	OF_DESC *ofd;


	if(fd<0)
		return 1;

	ofd = oft+fd;


	if(type==FSEEK_SET)
		ofd->offset = offset;
	else if(type==FSEEK_CUR)
		ofd->offset+= offset;
	else if(type ==FSEEK_END)
	{
			if(!ofd->fd.size)
				return 1;
			ofd->offset = ofd->fd.size-1;
			ofd->offset+=offset;
	}
	else
		return 1;
	return 0;
}



//write n bytes from the given buffer
//to the file described by fd
//returns number of bytes written
int fat_write(int fd, void *buf, int n)
{
	uint8 *br;
	OF_DESC *ofd;
	uint32 clust_num;
	uint32 clust_off;
	uint32 sec_off;
	uint32 nsec;
	uint32 count;
	int i=0;


	if(fd<0)
		return -1;

	ofd = oft+fd;


	//make sure file is open
	//and for write
	if(ofd->mode&OF_INV)
		return -1;
	if(!(ofd->mode&OF_WR))
		return -1;


	//if we need to grow the size
	if(ofd->offset+n >= ofd->fd.size)
		fat_truncate(&(ofd->fd),ofd->offset+n);

	//get the cluster of the curent offset
	clust_num = get_cluster_num(ofd->fd.start,ofd->offset);

	clust_off = CLUSTER_OFF(ofd->offset);
	sec_off = SECTOR_OFF(ofd->offset);

	br = get_ind_sector(clust_num,sec_off);

	count=0;

	//copy up to the next block
	for(i=ofd->offset%512; i<512 && n ; i++)
	{
		br[i] = *((uint8 *)buf);
		buf++;
		count++;
		n--;
	}

	put_ind_sector(clust_num,sec_off,br);

	ofd->offset+=count;

	if(!n)
		return count;

	//copy full blocks
	while(n>=512)
	{
		
		sec_off = SECTOR_OFF(ofd->offset);
		//check if se need to get from a new cluster
		if(clust_off!=CLUSTER_OFF(ofd->offset))
		{
			clust_off = CLUSTER_OFF(ofd->offset);
			clust_num = next_cluster(clust_num);
			if(clust_num>0xFFFFFF7 || clust_num <2) //end of file reached
				return count;
		}
		
		br = get_ind_sector(clust_num,sec_off);
		for(i=0;i<512;i++)
			br[i] = ((uint8 *)buf)[i];
		//put_ind_sector(clust_num,sec_off,buf);
		put_ind_sector(clust_num,sec_off,br);
		buf+=512;
		ofd->offset+=512;
		count+=512;
		n-=512;
	}


	//if last sector is in a new cluster
	sec_off = SECTOR_OFF(ofd->offset);
	if(clust_off!=CLUSTER_OFF(ofd->offset))
	{
		clust_off = CLUSTER_OFF(ofd->offset);
		clust_num = next_cluster(clust_num);
		if(clust_num>0xFFFFFF7 || clust_num <2) //end of file reached
			return count;
	}
	br = get_ind_sector(clust_num,sec_off);
	
	//copy the remainder
	for(i=0;i<n;i++)
		br[i] = ((uint8 *)buf)[i];
	put_ind_sector(clust_num,sec_off,br);

	count+=n;
	ofd->offset+=n;
	return count;
}



//read n bytes from the given file
int fat_read(int fd, void *buf, int n)
{
	uint8 *br;
	OF_DESC *ofd;
	uint32 clust_num;
	uint32 clust_off;
	uint32 sec_off;
	uint32 nsec;
	uint32 count;
	int i=0;

	if(fd<0)
		return -1;

	ofd = oft+fd;
	
	//file is not open
	if(ofd->mode&OF_INV)
		return -1;

	if(ofd->offset > ofd->fd.size)
		return 0;


	if(ofd->offset+n > ofd->fd.size)
		n = ofd->fd.size - ofd->offset;
	


	//get the cluster of the curent offset
	clust_num = get_cluster_num(ofd->fd.start,ofd->offset);


	clust_off = CLUSTER_OFF(ofd->offset);
	sec_off = SECTOR_OFF(ofd->offset);


	//load current sector into memory
	br = get_ind_sector(clust_num,sec_off);


	count=0;

	//read up to the next byte boundary
	//or unless we hit the count val
	for(i=ofd->offset%512;i<512 && n ;i++)
	{
		*(uint8 *)buf = br[i];
		buf++;
		count++;
		n--;
	}
	ofd->offset+=count;

	if(!n)
		return count;

	//copy n sectors	
	while(n>=512)
	{
		//get the next sector in the file
		sec_off = SECTOR_OFF(ofd->offset);
		//if next sec in in a new cluster, get new cluster num
		if(clust_off!=CLUSTER_OFF(ofd->offset))
		{ 
			clust_off = CLUSTER_OFF(ofd->offset);
			clust_num = next_cluster(clust_num);
			if(clust_num>0xFFFFFF7 || clust_num <2) //end of file reach
				return count;
		}
		br = get_ind_sector(clust_num,sec_off);
		for(i=0;i<512;i++)
		{
			((uint8 *)buf)[i] = br[i];
		}
		buf+=512;
		ofd->offset+=512;
		count+=512;
		n-=512;
	}

	sec_off = SECTOR_OFF(ofd->offset);
	if(clust_off!=CLUSTER_OFF(ofd->offset))
	{
		clust_off = CLUSTER_OFF(ofd->offset);
		clust_num = next_cluster(clust_num);
		if(clust_num>0xFFFFFF7 || clust_num <2) //end of file reached
			return count;
	}
	br = get_ind_sector(clust_num,sec_off);

	//copy leftover data
	for(i=0;i<n;i++)
		((uint8 *)buf)[i] = br[i];
	count+=n;
	ofd->offset+=n;
	return count;
}






