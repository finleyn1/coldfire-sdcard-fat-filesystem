#include "fat_type.h"
#include "fatfcntl.h"
#include "fatfops.h"
#include "fat_dir.h"


//can add more if necessary
char fts[128];
char *gtok[16];




int tokenize_path(char *path)
{
	int i=0;
	char *st;
	char *ed;

	//copy string
	for(i=0;i<128 && path[i];i++)
		fts[i] = path[i];
	fts[i]=0;
	fts[127]=0;//failsafe

	//go past starting slashes
	for(st = fts; *st=='/'; st++);

	if(!*st)
	{
		gtok[0]=0;
		return 0;
	}

	for(i=0;;i++)
	{
		gtok[i] = st;
		for(ed = st; *ed!='/' && *ed!='\0';ed++);
		if(*ed=='\0') //end of string
		{
			gtok[i+1]=0;
			return 0;
		}
		for(;*ed=='/';ed++)
			*ed=0;
		if(*ed==0)
		{
			gtok[i+1]=0;
			return 0;
		}
		st = ed;
	}
	

}


//convert src into space padded string in dst
int fat_st_conv(char *dst, char* src)
{
	int i=0;
	int j=0;
	for(i=0;i<11;i++)
		dst[i]=' ';

	//special cases for parent and self entries
	if(src[0]=='.')
	{
		if(src[1]=='\0')
		{
			dst[0]='.';
			return 0;
		}
		else if(src[1]=='.' && src[2]=='\0')
		{
			dst[0] = dst[1] ='.';
			return 0;
		}
	}


	for(i=0;i<8 && src[i] && src[i]!='.';i++)
	{
		if(src[i]>= 'a' && src[i]<= 'z')
			dst[i]=src[i]-=0x20; //make uppercase
		else if((src[i]>='A' && src[i]<='Z')||(src[i]>='0' && src[i]<='9'))
			dst[i]=src[i]; //legal characters
		else //break if unacceptable characters
			break;
	}

	if(src[i]!='.')
		return 0;


	//if name has an extension
	//point at next character
	src+=i+1;
	//copy extension
	for(j=0;j<3 && src[j];j++)
	{
		if(src[j]>= 'a' && src[j]<= 'z')
			dst[8+j]=src[j]-=0x20; //make uppercase
		else if((src[j]>='A' && src[j]<='Z')||(src[j]>='0' && src[j]<='9'))
			dst[8+j]=src[j]; //legal characters
		else //break if unacceptable characters
			break;
	}
}




int path_search(char *path,FAT_FILE *rt)
{
	FAT_FILE cur,ret;
	char fatname[11];
	int i;

	tokenize_path(path);


	if(!gtok[0] && fts[0]=='/')
	{
		*rt = root;
		return 0;
	}
	else if(!gtok[0])
	{
		*rt = cwd;
		return 0;
	}
	
	if(fts[0]=='/')
		cur = root;
	else
		cur = cwd;

	//go to end of path
	for(i=0;gtok[i];i++)
	{
		if(!(cur.attr&0x10))
		{
			return 1;
		}
		fat_st_conv(fatname,gtok[i]);
		if(dir_search(&cur,&ret,fatname))
			return 1;

		cur = ret;
	}


	*rt = cur;
	return 0;
}


//returns finfo for the next to last node 
//copies child name to ch_name
int par_search(char *path,FAT_FILE *rt,char *ch_name)
{
	FAT_FILE cur,ret;
	char fatname[11];
	int i;

	tokenize_path(path);

	if(!gtok[0])
		return 1;

	if(fts[0]=='/')
		cur = root;
	else
		cur = cwd;


	//corner case, if only item in path
	if(!gtok[1])
	{
		fat_st_conv(ch_name,gtok[0]);
		*rt = cur;
		return 0;
	}


	//go to end of path
	for(i=0;gtok[i+1];i++)
	{
		if(!(cur.attr&0x10))
			return 1;
		fat_st_conv(fatname,gtok[i]);
		if(dir_search(&cur,&ret,fatname))
			return 1;


		cur = ret;
	}

	fat_st_conv(ch_name,gtok[i]);
	*rt = cur;
	return 0;
}


//creates a directory at the path
int fat_mkdir(char *path)
{
	int i=0;
	char chname[11];
	FAT_FILE psearch,chsearch;

	//search for parent
	if(par_search(path,&psearch,chname))
		return 1;

	//if parent dir is not dir
	if(!(psearch.attr&0x10))
		return 1;

	//check if child entry exists in parent
	if(!dir_search(&psearch,&chsearch,chname))
		return 1; //file exists

	if(fatmkd(&psearch,chname))
		return 1;

	return 0;
}


//creates a from path
int fat_creat(char *path)
{
	int i=0;
	char chname[11];
	FAT_FILE psearch,chsearch;

	//search for parent
	if(par_search(path,&psearch,chname))
		return 1;

	//if parent dir is not dir
	if(!(psearch.attr&0x10))
		return 1;

	//check if child entry exists in parent
	if(!dir_search(&psearch,&chsearch,chname))
		return 1; //file exists

	if(fatmkf(&psearch,chname))
		return 1;

	return 0;
}

int fat_ls(char *path)
{
	FAT_FILE search;
	if(path[0]==0)
	{
		fat_list_dir(&cwd);
		return 0;
	}
	
	if(path_search(path,&search))
		return 1;

	fat_list_dir(&search);
	return 0;
}


int fat_cd(char *path)
{
	FAT_FILE search;
	if(path_search(path,&search))
		return 1;

	cwd = search;
	return 0;
}

int fat_open(char *path,uint32 mode)
{
	FAT_FILE search;
	if(path_search(path,&search))
		return -1;

	return fat_openf(&search,mode);
}


//removes the file given by the path
int fat_rm(char *path)
{
	FAT_FILE search;

	//search for file
	if(path_search(path,&search))
		return 1;

	//can't rm not file
	if(search.attr&~0x20)
		return 1;


	//return result of delete	
	return fatrmf(&search);
}


//removes directory given with path
//only if empty
int fat_rmdir(char *path)
{
	FAT_FILE search;

	//search for file
	if(path_search(path,&search))
		return 1;

	//only remove if is a directory
	if(!(search.attr&0x10))
		return 1;

	return fatrmd(&search);
}


int fat_trunc(char *path,uint32 size)
{
	FAT_FILE search;

	//search for file
	if(path_search(path,&search))
		return 1;

	return fat_truncate(&search,size);
}

