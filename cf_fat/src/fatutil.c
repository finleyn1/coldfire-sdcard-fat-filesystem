#include "fat_type.h"
#include "fblock.h"
#include "fat_util.h"

struct fat_fs_info fat_fsinfo;

FAT_FILE root;
FAT_FILE cwd;


//gcc wanted this
void *memcpy(void *dest, void *src, uint32 n)
{
	uint32 i;
	for(i=0;i<n;i++)
		((uint8 *)dest)[i] = ((uint8 *)src)[i];

	return dest;
}


//compares a number of bytes
//returns:
//0 if match
//1 if not
int ncmp(char *a, char *b, int n)
{
	int i=0;
	for(i=0;i<n;i++)
		if(a[i]!=b[i])
			return 1;
	return 0;
}


int init_fat()
{
	uint8 *sec;
	FAT_BOOTSEC *boots;
	FAT32_EXTB  *ebs;

	
	dbputs("Get boot sector\n\r");

	
	boots = get_sector(0);

	dbputs("Got boot sector\n\r");

	ebs = (FAT32_EXTB *)(((uint8 *)boots)+0x24);

	if(ncmp(ebs->type_label,"FAT32   ",8))
	{
		dbputc(ebs->type_label[0]);
		dbputc(ebs->type_label[1]);
		dbputc(ebs->type_label[2]);
		dbputc(ebs->type_label[3]);
		dbputc(ebs->type_label[4]);
		return 1; //may not be a fat disk
	}

	dbputs("Verified as FAT32 partition\n\r");
		
	fat_fsinfo.cluster_size = boots->sectors_per_cluster;
	
	//fat starts after reserved sectors
	fat_fsinfo.fat_start = bswap16(boots->reserved_sectors);
	fat_fsinfo.table_size = bswap32(ebs->table_size);

	fat_fsinfo.root_cluster = bswap32(ebs->root_cluster);

	root.start = fat_fsinfo.root_cluster;

	fat_fsinfo.data_start = fat_fsinfo.fat_start;

	fat_fsinfo.data_start += fat_fsinfo.table_size*boots->table_count;

	root.attr=0x10;
	root.size=0;
	root.dirsec=0;
	root.parcluster = root.start;

	cwd = root;


	return 0;

}
