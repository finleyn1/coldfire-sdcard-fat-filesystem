#include "fblock.h"

//cache entries in data segment
struct fc_ent cacheblk[N_CACHE];

//head of the cache list
struct fc_ent *cachel;

//sd block operation functions
extern int sd_read_blk(uint32 blkn, uint8 *buf);
extern int sd_write_blk(uint32 blkn, uint8 *buf);


//these functions need to be modified for different
//block devices for now they always call SD ops
int read_sector(uint32 sec, uint8 *data)
{
	sd_read_blk(sec,data);
}

int write_sector(uint32 sec, uint8 *data)
{
	sd_write_blk(sec,data);
}



//initializes the cache list
int init_cache()
{
	int i=0;

	for(i=0;i<N_CACHE;i++)
	{
		cacheblk[i].flag=0;
		cacheblk[i].num=0;
		cacheblk[i].next=cachel;
		cachel = cacheblk+i;
	}
}


//write any dirty entries to the disk
//mark entries as clean as an effect
int sync_cache()
{
	struct fc_ent *cp;

	//go through each item in list
	for(cp =cachel;cp;cp=cp->next)
	{
		//if entry is valid and dirty
		if((cp->flag&F_VALID) && (cp->flag&F_DIRTY))
		{
			//write to disk
			write_sector(cp->num,cp->sec);
			//clear dirty flag
			cp->flag&=~F_DIRTY;
		}
	}
}

//returns a pointer to a buffer containing 
//the data of the requested 
void *get_sector(uint32 sector)
{
	struct fc_ent *cp;
	struct fc_ent *tmp;
	int i=0;


	//if searched item is at front of list
	if(cachel->flag & F_VALID)
		if(cachel->num==sector)
			return cachel->sec;

	
	for(cp = cachel; cp->next; cp = cp->next)
	{
		if(cp->next->flag & F_VALID)
			if(cp->next->num ==sector)
			{
				//take item out of list
				tmp = cp->next;
				cp->next = tmp->next;
			
				//place at front	
				tmp->next = cachel;
				cachel = tmp;
		
				return cachel->sec;
			}
	}


	//item was not in list

	//take item off end of list
	for(cp = cachel->next; cp->next->next; cp = cp->next);

	
	tmp = cp->next;
	cp->next = 0; //new end of list

	//write if contents are dirty
	if((tmp->flag & F_VALID) && (tmp->flag & F_DIRTY) )
	{
		write_sector(tmp->num,tmp->sec);
		tmp->flag = F_VALID;
	}

	//get the new sector
	//then place at front of cache
	read_sector(sector,tmp->sec);
	tmp->num = sector;
	tmp->flag = F_VALID;
	tmp->next = cachel;
	cachel = tmp;

	return cachel->sec;
}



//writes the given data sector
//updates the cache if entry is in cache
//otherwise writes to the disk
//does not modify order in cache yet
int put_sector(uint32 sector, void *data)
{

	struct fc_ent *cp;
	struct fc_ent *tmp;
	int i=0;

	for(cp = cachel; cp ;cp = cp->next)
	{
		if(cp->flag & F_VALID)
			if(cp->num==sector)
			{
				for(i=0;i<512;i++)
					cp->sec[i] = ((uint8 *)data)[i];

				//mark as dirty
				cp->flag |= F_DIRTY;
				return 0;
			}
	}

	//not in cache, we will write directly
	write_sector(sector,data);
	return 0;
}



