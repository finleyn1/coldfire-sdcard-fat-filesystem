#include "fcluster.h"


//gets a sector, indexed from start of the given cluster
void *get_ind_sector(uint32 cluster, uint32 sector)
{
	uint32 abs_sec;
	abs_sec = fat_fsinfo.data_start +
			(cluster-2)*fat_fsinfo.cluster_size +
			sector;

	return get_sector(abs_sec);
}

//puts the given sector
int put_ind_sector(uint32 cluster, uint32 sector, void *data)
{
	uint32 abs_sec;

	abs_sec = fat_fsinfo.data_start +
			(cluster-2)*fat_fsinfo.cluster_size +
			sector;

	put_sector(abs_sec,data);	
}

//returns the next cluster in the fat chain
uint32	next_cluster(uint32 cluster)
{
	uint32	*blk;
	blk = get_sector(fat_fsinfo.fat_start+(cluster/128));

	return	bswap32(blk[cluster%128]);
}



//adds a cluster to the end of the chain
//starting at the given cluster
//returns allocated cluster num if success
//zero if failure
uint32	add_cluster(uint32 cluster)
{
	uint32 cur,next;
	uint32 *blk;
	uint32 alc;
	uint32 sec;

	if(cluster<=2 || cluster>=0x0FFFFFF8)
		return 0; //invalid cluster num
	
	cur = cluster;

	alc = alloc_cluster();
	if(alc==0)
		return 0;
	
	for(;;)
	{
		next = next_cluster(cur);
		if(next>0x0FFFFFF8)
		{
			sec = fat_fsinfo.fat_start;
			sec +=cur/128;
			blk = get_sector(sec);
			blk[cur%128] = bswap32(alc);
			put_sector(sec,blk);

			sec+=fat_fsinfo.table_size;
			blk = get_sector(sec);
			blk[cur%128] = bswap32(alc);
			put_sector(sec,blk);
			return alc;
		}
		cur = next;
	}
	free_cluster(alc);
	return 0;
}


int rem_cluster(uint32 start)
{
	uint32 cur,next;
	uint32 *blk;
	uint32 sec;

	if(start<=2 || start>=0x0FFFFFF8)
		return 1;

	if(next_cluster(start)>=0x0FFFFFF8);
		return 1;

	cur = start;
	for(;;)
	{
		next = next_cluster(cur);
		if(next_cluster(next)>=0x0FFFFFF8)
		{
			free_cluster(next);
			sec = fat_fsinfo.fat_start;
			sec += cur/128;
			blk = get_sector(sec);
			blk[cur%128] = bswap32(0x0FFFFFF8);
			put_sector(sec,blk);
			
			sec+=fat_fsinfo.table_size;

			blk = get_sector(sec);
			blk[cur%128] = bswap32(0x0FFFFFF8);
			put_sector(sec,blk);
			return 0;
		}
		cur = next;
	}
	return 1;


}


//allocates the next free cluster from the FAT
//marks the cluster as end of chain
//returns the number of the cluster
uint32 alloc_cluster()
{
	uint32 *blk;
	uint32 sec;
	uint32 n;
	int  i=0;
	n=0;
	
	//go from first sector in fat to 
	//sector 512
	for (	sec = fat_fsinfo.fat_start;
		sec < fat_fsinfo.fat_start+512;
	    	sec++)
	{
		blk = get_sector(sec);
		for(i=0;i<128;i++,n++) //128 long words in sector
		{
			//don't alloc first 3 clusters
			if(!blk[i] && n>=3)
			{
				//mark as EOC
				blk[i] = bswap32(0x0FFFFFF8);
				//update FAT
				put_sector(sec,blk);

				//update FAT copy
				sec+=fat_fsinfo.table_size;
				blk = get_sector(sec);
				blk[i] = bswap32(0x0FFFFFF8);
				put_sector(sec,blk);
				return n;
			}
		}
	}	

	return 0;
}


//frees the given cluster
int free_cluster(uint32 cluster)
{
	uint32 *blk;
	uint32 sec;
	
	if(cluster < 3 || cluster>=0x0FFFFFF8)
		return 1;


	sec = fat_fsinfo.fat_start;
	sec += cluster/128;


	blk = get_sector(sec);

	
	//mark as free
	blk[cluster%128]=0;
	put_sector(sec,blk);

	sec += fat_fsinfo.table_size;

	blk = get_sector(sec);
	blk[cluster%128]=0;
	put_sector(sec,blk);
	

	return 0;

}



//return the cluster number of the byte offset for a file
uint32 get_cluster_num(uint32 start, uint32 offset)
{
	uint32 cur;
	uint32 num;
	uint32 i;

	cur = start;	
	num = CLUSTER_OFF(offset);

	for(i=0;i<num;i++)
	{
		cur = next_cluster(cur);
		if(cur<2 || cur>=0x0FFFFFF8)
			return 0;
	}
	return cur;
}
