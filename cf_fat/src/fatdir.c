#include "fat_dir.h"
#include "fatfops.h"
#include "fcluster.h"



int dir_search(FAT_FILE *par,FAT_FILE *fin, char *name)
{
	
	
	int i;
	uint32 secind;
	uint32 dir_cluster;
	uint8  *dirblk;
	uint8  nc;
	FAT_DIRENT *dir;



	for(secind =0;secind<fat_fsinfo.cluster_size;secind++)
	{
		dirblk = get_ind_sector(par->start,secind);
		
		dir = (FAT_DIRENT *)dirblk;

		for(i=0;i<512/sizeof(FAT_DIRENT);i++)
		{
			nc = dir[i].name[0];

			if(!nc)
				return 1;

			

			if(nc==0xe5 || nc==0x05 || dir[i].attr==0x0F)
				continue;

			if(!ncmp(dir[i].name,name,11))
			{
				dir_cluster = bswap16(dir[i].cluster_high);
				dir_cluster <<=16;
				dir_cluster |= bswap16(dir[i].cluster_low);

				if(nc==0x2E && dir_cluster==0)
				{
					fin->start = fat_fsinfo.root_cluster;
					fin->size=0;
					fin->attr=0x10;
					fin->dirsec=0;
					fin->diroff=0;
					fin->parcluster = fin->start;
					return 0;
				} 

				fin->start = dir_cluster;
				fin->size = bswap32(dir[i].size);
				fin->attr = dir[i].attr;
				fin->dirsec = 
						fat_fsinfo.data_start + 
						(par->start-2)*fat_fsinfo.cluster_size
						+ secind;
				fin->diroff = i*sizeof(FAT_DIRENT);
				fin->parcluster = par->start;
				return 0;
			}

			
		}
	}
		return 1;


}





int fatmkf(FAT_FILE *par, char *name)
{
	
	uint32 secind;
	uint8  *dirblk;
	uint8  i;
	uint8  j;
	uint8  nc;
	FAT_DIRENT * dir;

	//go through sectors in cluster
	for(secind=0;secind<fat_fsinfo.cluster_size;secind++)
	{
		dirblk = get_ind_sector(par->start,secind);
	
		dir = (FAT_DIRENT *)dirblk;
		
		//go through direntries
		for(i=0;i<512/sizeof(FAT_DIRENT);i++)
		{
			nc = dir[i].name[0];


			//if entry is unallocated 
			if(nc==0 || nc==0xe5 || nc==0x05)
			{
				for(j=0;j<11;j++)
					dir[i].name[j]=name[j];
				
				dir[i].cluster_high=0;
				dir[i].cluster_low=0;
				dir[i].size=0;
				dir[i].attr=0x20;
	
				put_ind_sector(par->start,secind,dirblk);

				return 0;
				if(nc)
					return 0;	
				break;
			}

		
		}
		if(nc==0)
			break;
	

	}

	/******
	//since we used the end of dir
	//try to mark next entry as end of the dir
	if(nc==0)
	{
		if(i+1 == 512/sizeof(FAT_DIRENT))
		{
			//we cannot go further.........
			if(secind==fat_fsinfo.cluster_size)
				return 0;
			i=0;
			secind++;
		}
		else
			i++;

		dirblk = get_ind_sector(par->start,secind);
		dir = (FAT_DIRENT *)dirblk;
		dir[i].name[0]=0; //mark as end of dir
		put_ind_sector(par->start,secind,dirblk);
		return 0;
	}
	******/
	return 1; 
}

int fatmkd(FAT_FILE *par, char *name)
{
	uint32 alc;
	uint32 secind;
	uint8  *dirblk;
	uint8  i;
	uint8  j;
	uint8  nc;

	FAT_DIRENT *dir;

	//allocate cluster for the direcotry
	alc = alloc_cluster();

	//go through directory's first block	
	for(secind=0;secind<fat_fsinfo.cluster_size;secind++)
	{

		//get the current sector in block 
		dirblk = get_ind_sector(par->start,secind);

		dir = (FAT_DIRENT *)dirblk;
		for(i=0;i<512/sizeof(FAT_DIRENT);i++)
		{

			nc =dir[i].name[0];

			if(nc==0 || nc==0xe5 || nc==0x05)
			{

				//copy name and space pad
				for(j=0;j<8;j++)
					dir[i].name[j]=name[j];
				for(j=0;j<3;j++)
					dir[i].name[8+j]=' ';

				//store upper word
				dir[i].cluster_high =
						bswap16((alc>>16)&0xFFFF);

				//store lower word
				dir[i].cluster_low = 
						bswap16((alc&0xFFFF));
				
				//dir size must be zero
				dir[i].size=0;
				//mark as dir
				dir[i].attr=0x10;

				dir[i].reserved=0;
				dir[i].creation_time=0;
				dir[i].t_creat=0;
				dir[i].d_creat=0;
				dir[i].d_acc=0;
				dir[i].mod_time=0;
				dir[i].mod_date=0;
			
				//write sector back to disk
				put_ind_sector(par->start,secind,dirblk);

				//initialize the new directory's cluster
				init_fat_dir(alc,par);
				return 0;
				if(nc)
					return 0;

				break;
			}

		}
		if(nc==0)
			break;

	}
	/*****
	if(nc==0)
	{
		if(i+1 == 512/sizeof(FAT_DIRENT))
		{
			//we cannot go further.........
			if(secind==fat_fsinfo.cluster_size)
				return 0;
			i=0;
			secind++;
		}
		else
			i++;

		dirblk = get_ind_sector(par->start,secind);
		dir = (FAT_DIRENT *)dirblk;
		dir[i].name[0]=0; //mark as end of dir
		put_ind_sector(par->start,secind,dirblk);
		return 0;
	}
	*****/
	free_cluster(alc);
	return 1;

}

int init_fat_dir(uint32 alcluster, FAT_FILE *par)
{
	uint32 i=0;	
	uint32  j=0;
	uint8  *dirblk;

	FAT_DIRENT *dir;


	//zero out the allocated cluster
	for(i=0;i<fat_fsinfo.cluster_size;i++)
	{
		dirblk = get_ind_sector(alcluster,i);
		for(j=0;j<128;j++)
			((uint32 *)dirblk)[j]=0;
		put_ind_sector(alcluster,i,dirblk);
	}
	

	//get the first sector
	dirblk = get_ind_sector(alcluster,0);

	dir = (FAT_DIRENT *)dirblk;


	//add entry for '.'
	dir->name[0] = '.';
	for(i=0;i<10;i++)
		dir->name[1+i]=' ';
	dir->attr=0x10;
	dir->cluster_high = bswap16((alcluster>>16)&0xFFFF);
	dir->cluster_low = bswap16((alcluster&0xFFFF));
	dir->size=0;

	dir++;

	//add entry for '..'
	dir->name[0] = '.';
	dir->name[1] = '.';
	for(i=0;i<9;i++)
		dir->name[2+i]=' ';
	dir->attr=0x10;
	if(par->start==fat_fsinfo.root_cluster)
	{
		dir->cluster_high =0;
		dir->cluster_low =0;
	}
	else
	{
		dir->cluster_high = bswap16((par->start>>16)&0xFFFF);
		dir->cluster_low = bswap16((par->start&0xFFFF));
	}
	dir->size=0;

	//write sector back
	put_ind_sector(alcluster,0,dirblk);
	
	return 0;	
}



int fatrmd(FAT_FILE *fin)
{
	uint8  *dirblk;
	uint8  *cp;
	uint8  nc;
	int i=0;
	uint32	secind;

	FAT_DIRENT *dir;


	//embedded limitattion
	//only look at first cluster of a director
	for(secind=0;secind<fat_fsinfo.cluster_size; secind++)
	{
		dirblk = get_ind_sector(fin->start,secind);
		dir = (FAT_DIRENT *)dirblk;
		for(i=0;i<512/sizeof(FAT_DIRENT);i++)
		{
			nc = dir[i].name[0];

			if(nc==0)
			{
				free_cluster(fin->start);
				dirblk = get_sector(fin->dirsec);
				dir = (FAT_DIRENT *)(dirblk+fin->diroff);
				dir->name[0]=0xE5;
				dir->cluster_high=0;
				dir->cluster_low=0;	
				put_sector(fin->dirsec,dirblk);
				return 0;

			}
			if(nc==0xe5 || nc==0x05 || nc==0x2E)
				continue;
			else if(dir[i].attr==0x0F)
				continue;


			return 1;
		}

	}

	//we're not gonn check other clusters
	//thus not assuming the dir is empty
	//we'll report error
	return 1;
}



int fatrmf(FAT_FILE *fin)
{
	uint8 *sec;
	FAT_DIRENT *dir;


	//only rm reg_file
	if(fin->attr&~(0x20))
		return 1;

	
	//set file size to zero, freeing all clusters
	fat_truncate(fin,0);

	//get the sector with the file's
	//dir entry
	sec = get_sector(fin->dirsec);
	dir = (FAT_DIRENT *)(sec+fin->diroff);

	//mark as a free dir entry, init cluster field
	dir->name[0]=0xE5;
	dir->cluster_high=0;
	dir->cluster_low=0;
	put_sector(fin->dirsec,sec);

	return 0;
}



fat_list_dir(FAT_FILE *par)
{
	uint8 *dirblk;
	int i=0;	
	int j=0;
	int secind=0;
	uint8 nc;
	FAT_DIRENT *dir;	

	dbputs("\n\r");
	for(secind=0;secind<fat_fsinfo.cluster_size;secind++)
	{
		dirblk = get_ind_sector(par->start,secind);
	
		dir = (FAT_DIRENT *)dirblk;
		for(i=0;i<512/sizeof(FAT_DIRENT);i++)
		{
			nc = dir[i].name[0];
	
			if(!nc)
				return 0;
		
			if(nc==0xe5)
				continue;
			else if(nc==0x05)
				continue;
			else if(dir[i].attr==0xF)
				continue;

			if(dir[i].attr&0x20)
				dbputs("f: ");

			if(dir[i].attr&0x10)
				dbputs("d: ");

			for(j=0; j<8;j++)
			{
				if(dir[i].name[j]==' ')
					break;
				dbputc(dir[i].name[j]);
			}
			
			if(dir[i].name[8]!=' ')
			{
				dbputc('.');
				for(j=0;j<3;j++)
					dbputc(dir[i].name[8+j]);
			}
			dbputs("\n\r");
		}
	}
	return 0;
	
}




