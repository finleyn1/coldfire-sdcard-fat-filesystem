
.PHONY: cf_sd cf_fat


CC = m68k-linux-gnu-gcc
AS = m68k-linux-gnu-as
LD = m68k-linux-gnu-ld
OBC = m68k-linux-gnu-objcopy
OBD = m68k-linux-gnu-objdump

CFLAG = -I./include -I./cf_fat/include -I./cf_sd/include -mcpu=52259 -falign-functions=4
ASFLAG = -march=isaaplus -mcpu=52259 --register-prefix-optional




all: testprog vect startup dbio init cf_sd cf_fat
	cp ./cf_sd/obj/cf_sd.o ./obj/cf_sd.o
	cp ./cf_fat/obj/cf_fat.o ./obj/cf_fat.o
	$(LD) -T cf_filetest.ld obj/* -o bin/cf_filetest.elf
	$(OBC) -O binary bin/cf_filetest.elf bin/cf_filetest.bin




testprog:
	$(CC) $(CFLAG) -c src/filesystest.c -oobj/test.o

init:
	$(CC) $(CFLAG) -c src/$@.c -oobj/$@.o

dbio:
	$(CC) $(CFLAG) -c src/$@.c -oobj/$@.o

vect:
	$(AS) $(ASFLAG) -c src/$@.s -oobj/$@.o

startup:
	$(AS) $(ASFLAG) -c src/$@.s -oobj/$@.o


cf_sd:
	make -C ./cf_sd

cf_fat:
	make -C ./cf_fat



clean:
	make -C ./cf_sd clean
	make -C ./cf_fat clean
	rm -f obj/*
	rm -f bin/*
