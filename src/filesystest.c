#include "cf_type.h"
#include "dbio.h"
#include "init.h"
#include "qspi.h"
#include "sd_qspi.h"
#include "fat_type.h"
#include "fblock.h"
#include "fat_dir.h"
#include "fatfcntl.h"
#include "fatfops.h"




//placeholder byteswaps
//should replace with better assembly versions
uint32 bswap32(uint32 a)
{

	uint32 out;
	out =  (a>>24)&0xFF;
	out |= (a>>8) &0xFF00;
	out |= (a<<8) &0xFF0000;
	out |= (a<<24)&0xFF000000;
	return out;
}


uint16 bswap16(uint16 a)
{
	uint16 out;
	out =  ((a)>>8)&0xFF;
	out |= (a&0xFF)<<8;
	return out;
}


int main(void)
{

	int i=0;
	int j=0;
	int nread;
	int fd;
	int ret;
	char ibuff[128];
	char c;

	scm_init();
	pll_init(3);


	init_dbled();
	set_dbled(0);

	init_debug_uart();
	qspi_init();

	if(sd_init())
	{
		dbputs("Failed to Init SD card\n\r");
		for(;;);
	}

	dbputs("Successfully init SD card\n\r");
	dbputs("Attempt to read FAT filesystem\n\r");

	init_cache();

	if(init_fat())
	{
		dbputs("Unable to identify as FAT32 DISK\n\r");
		for(;;);
	}
	dbputs("FAT Read Success\n\r");

	//initialize file table
	init_oft();

	for(;;)
	{
		dbputs("\n\r(c)hdir\t(l)ist\tc(a)t\t(e)cho\n\r");
		dbputs("mk(d)ir\tmk(f)ile\tx:rm file\tX:rmdir\n\r");
		dbputs("Enter Command: ");
		if(!dbgets(ibuff,128))
			break;
		c = ibuff[0];
		dbputs("\n\rEnter Path: ");
		if(!dbgets(ibuff,128))
			break;
		ret =0;
		switch(c)
		{
			case 'c': //cd
				fat_cd(ibuff); break;
			case 'l': //ls
				fat_ls(ibuff); break;
			case 'a': //cat
				ret = fat_cat(ibuff); break;
			case 'e': //echo
				ret = fat_echo(ibuff); break;
			case 'd':
				ret = fat_mkdir(ibuff); break;
			case 'f':
				ret = fat_creat(ibuff); break;
			case 'x':
				ret = fat_rm(ibuff); break;
			case 'X':
				ret = fat_rmdir(ibuff); break;
			default:
				dbputs("unknown command\n\r"); break;
		}
		if(ret)
			dbputs("Command error...\a\n\r");
	}


	dbputs("\n\n\rDone! Syncing cache...\n\r");
	sync_cache();

	for(;;)
	{
		set_dbled(j&0xF);
		j++;
		for(i=0;i<1000000;i++);
	}

}




int fat_cat(char *path)
{
	int fd;
	int size;
	int i=0;
	char catb[32];
	fd = fat_open(path,OF_RD);

	if(fd<1)
		return 1;
	dbputs("\n\r\n\r");
	for(;;)
	{
		size = fat_read(fd,catb,2);
		if(size<0)
		{
			fat_close(fd);
			return 1;
		}
		if(size==0)
			break;
		for(i=0;i<size;i++)
			dbputc(catb[i]);
	}
	fat_close(fd);
	return 0;
}

int fat_echo(char *path)
{
	int fd;
	int size;
	int i;
	char linebuf[128];
	

	fd = fat_open(path,OF_WR);

	if(fd<1)
		return 1;
	
	for(;;)
	{
		dbputs("Enter A line (Ctl+D to exit): ");
		if(dbgets(linebuf,128))
		{
			for(i=0;linebuf[i];i++);
			size = fat_write(fd,linebuf,i);
			fat_write(fd,"\n\r",2);
			if(fat_write==-1)
				return 1;
			dbputs("\n\r");
			continue;
		}
		dbputs("\n\r");
		break;
	}
	dbputs("\n\r");

}





