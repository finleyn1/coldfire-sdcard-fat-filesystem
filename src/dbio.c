#include "dbio.h"
#include "cf_type.h"

#define VAL_G1 0

void init_dbled()
{
	MCF_GPIO_PORTTC=3;
	MCF_GPIO_DDRTTC=0xFF;
	MCF_GPIO_PTCPAR=0;
}

void set_dbled(uint8 val)
{
	MCF_GPIO_PORTTC = val&0xF;
}

void init_debug_uart()
{
	MCF_UART_UCR = 0x20;
	MCF_UART_UCR = 0x30;
	MCF_UART_UCR = 0x10;


	MCF_UART_UMR = 0x53;
	MCF_UART_UMR = 0x7;

	MCF_UART_UCSR = 0xdd;
	MCF_UART_UIMR = 0;

	MCF_UART_UBG1=0;
	MCF_UART_UBG2=17;

	MCF_UART_UCR = 4|1;

	
	//assign pins to UART
	MCF_GPIO_PUAPAR= 1 | 4;

}


void dbputc(char c)
{
	while(!(MCF_UART_USR&4));
	MCF_UART_UTB = c;
}

void dbputs(char *s)
{
	for(;*s;s++)
		dbputc(*s);
}

char dbgetc(void)
{
	while(!(MCF_UART_USR&1));
	return MCF_UART_URB;
}

char *dbgets(char *buf, int n)
{
	int i=0;
	char c;
	for(i=0;i<n;i++)
	{
		c = dbgetc();
		if(c==4)
			return 0;
		else if(c=='\r')
		{
			buf[i]=0;
			break;
		}
		else if(c==0x7F)
		{
			if(i==0)
			{
				i--;
				continue;
			}
			buf[i]=0;
			buf[i-1]=0;
			dbputc(0x8);
			dbputc(' ');
			dbputc(0x8);
			i-=2;
			continue;
		}
		dbputc(c);
		buf[i]=c;
	}
	if(i==n)
		buf[i-1]=0;
	return buf;
}


void crummynum(uint32 a)
{
	int i=0;
	uint8 c;
	for(i=0;i<8;i++)
	{
		c = (a>>(4*(7-i)))&0xF;
		if(c>=10)
			dbputc('a'+c-10);
		else
			dbputc('0'+c);
	}
	dbputs("\n\r");
}
