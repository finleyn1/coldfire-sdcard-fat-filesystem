#ifndef SD_QSPI_H
#define SD_QSPI_H


#define SD_INIT_TOUT	4096
#define SD_READ_TIMEOUT	2048

int sd_init();
int sd_cmd0();
int sd_r1_cmd(uint8 cmd, uint8 *resp);
int sd_read_blk(uint32 blkn, uint8 *buf);
int sd_write_blk(uint32 blkn, uint8 *buf);






#endif
