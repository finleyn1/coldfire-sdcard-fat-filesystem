#ifndef QSPI_H
#define QSPI_H

#include "cf_type.h"

#define QSPI_BASE	0x40000340

//QSPI registers
#define QSPI_QMR	*((vuint16 *) QSPI_BASE)
#define QSPI_QDLYR	*((vuint16 *) (QSPI_BASE+4))
#define QSPI_QWR	*((vuint16 *) (QSPI_BASE+8))
#define QSPI_QIR	*((vuint16 *) (QSPI_BASE+0xc))
#define QSPI_QAR	*((vuint16 *) (QSPI_BASE+0x10))
#define QSPI_QDR	*((vuint16 *) (QSPI_BASE+0x14))


//QSPI GPIO pins
#define GPIO_PORTQS	*((vuint8 *)  0x4010000C)
#define GPIO_DDRQS	*((vuint8 *)  0x40100024)
#define GPIO_SETQS	*((vuint8 *)  0x4010003C)
#define GPIO_CLRQS	*((vuint8 *)  0x40100054)
#define GPIO_PQSPAR	*((vuint16 *) 0x4010006C)


#define QS_PAR0(x)	((x)&0x3)
#define QS_DOUT_GPIO	0
#define QS_DOUT_DOUT	1

#define QS_PAR1(x)	(((x)&0x3)<<2)
#define QS_DIN_GPIO	0
#define QS_DIN_DIN	4

#define QS_PAR2(x)	(((x)&0x3)<<4)
#define QS_CLK_GPIO	0
#define QS_CLK_CLK	0x10

#define QS_PAR3(x)	(((x)&0x3)<<6)
#define QS_CS0_GPIO	0
#define QS_CS0_CS0	0x40


#define QS_PAR4(x)	(((x)&0x3)<<8)

#define QS_PAR5(x)	(((x)&0x3)<<A)
#define QS_CS2_GPIO	0
#define QS_CS2_CS2	0x400

#define QS_PAR6(x)	(((x)&0x3)<<C)
#define QS_CS3_GPIO	0
#define QS_CS3_CS3	0x10000



void qspi_init();


void qspi_mode(uint8 mode);
void qspi_tx_size(uint8 size);
void qspi_set_baud(uint8 baud_div);

void qspi_set_clk_delay(uint8 del);
void qspi_set_dt_del(uint8 del);

void qspi_en_man_cs(int pol);
void qspi_en_auto_cs();
void qspi_set_cs(uint8 mask);

//send and receive one byte using polling only
int qspi_byte(uint8 tx_dat, uint8 *rx_dat, uint8 cs);


/****

PORTQS SQPI pins:
0: DOUT
1: DIN
2: CLK
3: CS0
4: CS1
5: CS2

****/



#endif
