#include "qspi.h"
#include "cf_type.h"
#include "sd_qspi.h"


//extern void dbputs(char *str);
//extern void crummynum(uint32 a);

uint32 sderr;
//puts sd (CS0) in spi mode ready to read/write data
// 0 if success, 1 if unable to init card
int sd_init()
{
	int i=0;	
	uint8 stat;

	qspi_set_baud(11);
	//set CS manual
	qspi_en_man_cs(1);

	//set set PHA=1 POL=1

	qspi_mode(3);

	if(sd_cmd0())
	{
		;
		//return 1;
	}

	for(i=0;i<SD_INIT_TOUT;i++)
	{
		sd_r1_cmd(55,0);
		if(sd_r1_cmd(41,&stat))
			continue;

		//sderr = 0|stat&0xFF;

		if(!(stat&1))
			return 0;
	} 
	return 1;
}



uint8 dat_cmd0[6] = { 0x40, 0, 0, 0, 0, 0x95};
uint8 dat_cmd1[6] = { 0x41, 0, 0, 0, 0, 1};


//send cmd0 and receive a response
//0 if success 1 if no response
int sd_cmd0()
{
	uint8 rxv;
	int i=0;
	//CS0 low
	qspi_set_cs(0xE);

	qspi_byte(0xFF,0,0);
	qspi_byte(0xFF,&rxv,0);

	//expected high only, maybe no card?	
	if(rxv!=0xFF)
	{
		qspi_set_cs(0xF);
		return 1;
	}

	//send the command
	for(i=0;i<6;i++)
		qspi_byte(dat_cmd0[i],0,0);


	for(i=0;i<8;i++)
	{
		qspi_byte(0xFF,&rxv,0);
		if(rxv!=0xFF)
			break;
	}

	//response timeout
	if(i==8)
	{
		qspi_set_cs(0xF);	
		return 1;
	}


	//rxv holds card response
	
	qspi_byte(0xFF,0,0);
	qspi_set_cs(0xF);	
	return 0;
}


//returns the response to an command
int sd_r1_cmd(uint8 cmd, uint8 *resp)
{

	uint8 rxv;
	int i=0;

	qspi_set_cs(0xE);


	qspi_byte(0xFF,0,0);
	
	qspi_byte(0x40 | cmd,0,0);

	for(i=0;i<4;i++)
		qspi_byte(0,0,0);

	qspi_byte(1,0,0);

	for(i=0;i<8;i++)
	{
		qspi_byte(0xFF,&rxv,0);
		if(!(rxv&0x80))
			break;
	}
	
	if(i==8)
	{
		qspi_set_cs(0xF);
		return 1;
	}

	
	if(resp)	
		*resp = rxv;

	qspi_byte(0xFF,0,0);
	qspi_set_cs(0xF);
	return 0;

}




int sd_read_blk(uint32 blkn, uint8 *buf)
{
	uint8 rv;
	int i=0;

	set_dbled(1);
		


	dbputs("DEBUG SD Read block : ");
	crummynum(blkn);

	blkn*=512;

	qspi_set_cs(0xE);

	qspi_byte(0x40|0x11,0,0);
	qspi_byte(0xFF&(blkn>>24),0,0);
	qspi_byte(0xFF&(blkn>>16),0,0);
	qspi_byte(0xFF&(blkn>>8),0,0);
	qspi_byte(0xFF&(blkn),0,0);

	//1 at end of command
	qspi_byte(1,0,0);

	
	for(i=0;i<8;i++)
	{
		qspi_byte(0xFF,&rv,0);
		if(rv!=0xFF)
			break;
	}

	if(i==8)
	{
		qspi_set_cs(0xF);
		set_dbled(0);
		return 1;
	}

	if(rv&0x7E)
	{
		qspi_set_cs(0xF);
		sderr = 0| (rv&0x7E);
		set_dbled(0);
		return 1;
	}


	for(i=0;i<SD_READ_TIMEOUT;i++)
	{
		qspi_byte(0xFF,&rv,0);
		if(rv!=0xFF)
			break;
	}

	if(i==SD_READ_TIMEOUT)
	{
		qspi_set_cs(0xF);
		set_dbled(0);
		return 1;
	}

	//if response was not a data token
	if(rv!=0xFE)
	{
		qspi_set_cs(0xF);
		set_dbled(0);
		return 1;
	}

	
	for(i=0;i<512;i++)
		qspi_byte(0xFF,buf+i,0);

	qspi_byte(0xFF,0,0);
	qspi_byte(0xFF,0,0);
	qspi_byte(0xFF,0,0);
	
	qspi_set_cs(0xF);

	dbputs("DEBUG SD Read Success\n\r");

	set_dbled(0);
	return 0;
}


int sd_write_blk(uint32 blkn, uint8 *buf)
{

	int i=0;
	uint8 rv;
	uint8 dresp;

	dbputs("DEBUG SD write BLK :");
	crummynum(blkn);

	blkn*=512;

	set_dbled(2);
	qspi_set_cs(0xE);

	qspi_byte(0x40|24,0,0);
	qspi_byte(0xFF&(blkn>>24),0,0);
	qspi_byte(0xFF&(blkn>>16),0,0);
	qspi_byte(0xFF&(blkn>>8),0,0);
	qspi_byte(blkn,0,0);
	qspi_byte(1,0,0);


	for(i=0;i<8;i++)
	{
		qspi_byte(0xFF,&rv,0);
		
		if(rv!=0xFF)
			break;
	}

	if(i==8)
	{
		qspi_set_cs(0xF);
		set_dbled(0);
		return 1;
	}

	//check response

	
	if(rv&0b01100100)
	{
		qspi_set_cs(0xF);
		set_dbled(0);
		return 1;
	}
	
	qspi_byte(0xFF,0,0);

	//denote start of data
	qspi_byte(0xFE,0,0);

	for(i=0;i<512;i++)
		qspi_byte(buf[i],0,0);

	//dummy checksum
	qspi_byte(0,0,0);
	qspi_byte(0,0,0);

	//get data response
	qspi_byte(0xFF,&dresp,0);

	rv = 0;

	//wait until the busy signal goes away
	do
	{
		qspi_byte(0xFF,&rv,0);
	} while(rv!=0xFF);

	qspi_byte(0xFF,0,0);

	qspi_set_cs(0xF);

	//0 if no errors
	//1 if errors
	dbputs("DEBUG SD Write Done\n\r");
	set_dbled(0);
	return ((dresp&0xF)!=5);

}







