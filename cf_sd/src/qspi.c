#include "qspi.h"

//inits qspi module
//configures clk and data pins
//does not confgure chip selects
void qspi_init()
{
	QSPI_QMR = 0x8000; //make sure in master mode
	QSPI_QAR = 0x20;   //point at command ram
	QSPI_QIR |= 0xF;    //clear interrupts if any, disable allints

	//assign qs pins to qspi data and clock
	GPIO_PQSPAR |=	QS_DOUT_DOUT	|
			QS_DIN_DIN	|
			QS_CLK_CLK;
}


//switches qspi to use GPIO driven CS pins
//if pol is 1, CS init high before driving pin
void qspi_en_man_cs(int pol)
{
	//set cs lines as input (high Z)
	GPIO_DDRQS &= ~0x78;

	//switch to GPIO
	// clear |13 12|11 10|9 8|7 6|

	GPIO_PQSPAR &= ~0x3FC0;

	//set level
	if(pol)
		GPIO_PORTQS |= 0x78;
	else
		GPIO_PORTQS &= ~0x78;

	//set as output
	GPIO_DDRQS |= 0x78;

}



//writes the value in the first 4 bits
//To the CS pins, pins take on 
//mask is [cs3, cs2, QS4, CS0] left is msb
void qspi_set_cs(uint8 mask)
{
	//CS PINS IN QS
	//bits 3 4? 5 6
	GPIO_SETQS =  mask<<3;
	//GPIO_CLRQS =  ((~mask)&0xF)<<3;
	GPIO_CLRQS = 0xFF & (mask)<<3;

	//GPIO_PORTQS &= ~((mask&0xF)<<3);
}


//restores CS pins to be controlled
//by the qspi module
//does not change the GPIO settings other than PAR
void qspi_en_auto_cs()
{
	//make lines high z
	GPIO_DDRQS &= ~0x78;

	//switch to auto cs
	GPIO_PQSPAR |=	QS_CS0_CS0	|
			QS_CS2_CS2	|
			QS_CS3_CS3;
}

void qspi_mode(uint8 mode)
{
	QSPI_QMR &= ~0x300;
	QSPI_QMR |= ((mode&0x3)<<8)&0x300;
}



void qspi_tx_size(uint8 size)
{
	QSPI_QMR &= ~0x3C00;
	QSPI_QMR |= ((size&0xF)<<10)&0x3C00;
}


void qspi_set_baud(uint8 baud_div)
{
	QSPI_QMR &= ~0xFF;
	QSPI_QMR |= (baud_div)&0xFF;
}



//set delay before clock assertion
void qspi_set_clk_delay(uint8 del)
{
	QSPI_QDLYR &= ~0x7F;
	QSPI_QDLYR |= (del&0x7F)<<8;
}


//set delay after transfer
void qspi_set_dt_del(uint8 del)
{
	QSPI_QDLYR &= ~0xFF;
	QSPI_QDLYR |= del;
}




//blocking byte transfer over qspi
int qspi_byte(uint8 tx_dat, uint8 *rx_dat, uint8 cs)
{
	uint16 temp;
	if(QSPI_QDLYR&0x8000)
		return 1;

	//one command queue entry
	QSPI_QAR = 0x20;
	QSPI_QDR = (cs&0x7)<<8;

	//one data queue entry
	QSPI_QAR = 0;
	temp = tx_dat&0xFF;
	QSPI_QDR = temp;


	//set begining and end to 0 entry
	//QSPI_QWR &= ~(0x4F0F);
	QSPI_QWR = 0x1000;


	QSPI_QIR|=0xF;

	//init transfer
	QSPI_QDLYR |= 0x8000;

	//wait for SPIF
	while( !(QSPI_QIR&0x1));

	//wait for transfer to finish
	while((QSPI_QDLYR&0x8000) );

	//clear flag
	QSPI_QIR|=01;

	QSPI_QAR = 0x10;
	if(rx_dat)
	{
		temp = QSPI_QDR;
		*rx_dat = temp&0xFF;
	}
	
	return 0;
}



/****
//up to 16 transfers in this function
void qspi_single_transfer(uint16_t tx_data *,uint16_t rx_data *, uint8 n, uint8 cs)
{
	int i=0;
	uint16 temp;

	temp = 0xC000;
	temp |= (cs&0x7)<<8;
	
	for(i=0;i<n;i++)
		QSPI_QDR = temp;

	QSPI_QAR = 0;

	for(i=0;i<16;i++)
		QSPI_QDR = tx_data[i];

	temp =  0x5000;
	temp |= (n&0xF)<<8;
	
	//ready to transmit
	QSPI_QIR |= 0xF;

	//wait for transfer to complete
	while(!(QSPI_QIR &0x1));
	
	QSPI_QAR = 0x10;
	for(i=0;i<n;i++)
		rx_data[i] = QSPI_QDR;
}

************/
