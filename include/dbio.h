#ifndef DBIO_H
#define DBIO_H

#include "cf_type.h"

#define DBUART_BASE	(0x40000200)

#define MCF_UART_USR	*((vuint8 *) (DBUART_BASE+4))
#define MCF_UART_UCR	*((vuint8 *) (DBUART_BASE+8))
#define MCF_UART_UMR	*((vuint8 *) DBUART_BASE)
#define MCF_UART_UTB	*((vuint8 *) (DBUART_BASE+12))
#define MCF_UART_URB	*((vuint8 *) (DBUART_BASE+12))


#define MCF_UART_UCSR	*((vuint8 *) (DBUART_BASE+4))
#define MCF_UART_UIMR	*((vuint8 *) (DBUART_BASE+20))
#define MCF_UART_UBG1	*((vuint8 *) (DBUART_BASE+24))
#define MCF_UART_UBG2	*((vuint8 *) (DBUART_BASE+28))



#define MCF_GPIO_PORTTC *((vuint8 *) (0x4010000F))
#define MCF_GPIO_DDRTTC *((vuint8 *) (0x40100027))
#define MCF_GPIO_PTCPAR *((vuint8 *) (0x4010005F))

#define MCF_GPIO_PUAPAR *((vuint8 *) 0x40100071)


#ifndef VAL_G2
	#define VAL_G2 18
#endif


void init_debug_uart();

void dbputc(char c);

char dbgetc(void);

char *dbgets(char *buf, int n);

void dbputs(char *s);

void crummynum(uint32 a);



void init_dbled();
void set_dbled(uint8 val);



#endif
