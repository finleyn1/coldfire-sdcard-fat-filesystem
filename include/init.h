#ifndef INIT_H
#define INIT_H

#include "cf_type.h"
#include "MCF52259_CLOCK.h"


#define MCF_SCM_IPSBAR		(*(vuint32*)0x40000000)
#define MCF_SCM_RAMBAR		(*(vuint32*)0x40000008)

#define MCF_SCM_RAMBAR_BDE	(0x200)



int pll_init(uint8 speed);
int scm_init();




#endif
